package tasks

import (
	"database/sql"
	"encoding/json"
	"errors"
	"math"
	"strings"

	"git.mo-mar.de/masp/users"
)

// erstellen von Tasks
// Als Nutzer möchte ich Aufgaben erstellen können, damit ich die Aufgaben des Projekts in der Software darstellen kann.

//➜ Eine Aufgabe wurde erfolgreich erstellt,
//   wenn es nach der Erstellung einer Aufgabe in der Oberfläche eine neue Aufgabe in der Liste angezeigt wird
//   und in der Datenbank ein Eintrag für die neue Aufgabe angelegt wurde.

//Task ist struct was einen Task definiert
//GlobalID weißt dem Task eine ID zu
//AssignedTo weißt der Task einen Verantwortlichen zu
//Titlt gibt der Task einen Namen zum Unterscheiden
//Parent weißt dem Task eine Oberaufgabe, wenn vorhanden, zu
//Desciption beschreibt die Task
//DueDate gibt den der Task ein Enddatum zu
type Task struct {
	GlobalID    int               `csv:"GlobalID"`
	AssignedTo  []string          `csv:"AssignedTo"`
	Title       string            `csv:"Title"`
	Parent      int               `csv:"Parent"`
	Description string            `csv:"Description"`
	DueDate     string            `csv:"DueDate"`
	Metadata    map[string]string `csv:"Metadata"`
	Order       int               `csv:"Order"`
	Status      string            `csv:"Status"`
	Link        string
}

//Save speichert einen Task in die Datenbank
//Sollte der Task schon vorhanden sein dann werden die Einträge beim Ausführen dieser Funktion aktualisiert
func (t *Task) Save() error {
	//Siehe Doku
	l := m.Log("Save Task")
	var err error
	var result sql.Result
	//schaut ob titel nur aus leerzeichen besteht oder leer ist --> wenn ja dann error
	if strings.TrimSpace(t.Title) == "" {
		return errors.New("Input Error: Title is empty")
	}
	// schauen ob es ein neuer task oder ein bestehender ist
	AssignedTo, _ := json.Marshal(t.AssignedTo)
	if t.GlobalID == 0 {
		t.GlobalID = m.GetNewGlobalID()
		//packen task in Datenbank --> create
		result, err = m.GetDatabase().Exec("INSERT INTO tasks (global_ID, assigned_to, title, parent, description, due_date, `order`, status_task) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", t.GlobalID, AssignedTo, t.Title, t.Parent, t.Description, t.DueDate, t.Order, t.Status)
	} else {
		//Update Task in Datenbank --> bearbeiten
		result, err = m.GetDatabase().Exec("UPDATE tasks SET assigned_to = ?, title = ?, parent =? , description = ?, due_date = ?, `order` = ?, status_task = ? WHERE global_ID = ?", AssignedTo, t.Title, t.Parent, t.Description, t.DueDate, t.Order, t.Status, t.GlobalID)
	}
	if err != nil {
		// Fehler behandeln
		l.Error("Database Error: %s", err)
		return err
	}
	if n, err := result.RowsAffected(); err != nil || n == 0 {
		// Keine Zeile betroffen
		return errors.New("Not Found Error: Task does not exists")
	}

	for _, meta := range GetAllMeta(t.GlobalID) {
		if _, ok := t.Metadata[meta.OptionalMetadata]; !ok {
			err := meta.deleteMeta()
			if err != nil {
				l.Warn("Metadata Error: %s", err)
			}
		}
	}
	for meta, value := range t.Metadata {
		err := (&Meta{GlobalID: t.GlobalID, OptionalMetadata: meta, ValueMetadata: value}).addMeta()
		if err != nil {
			l.Warn("Metadata Error: %s", err)
		}
	}

	return nil
}

//Delete führt das Löschen einer Task aus der Datenbank durch
func (t *Task) Delete() error {
	//Siehe Doku
	l := m.Log("Delete Task")

	result, err := m.GetDatabase().Exec("DELETE FROM tasks WHERE global_ID = ?", t.GlobalID)
	if err != nil {
		// Fehler behandeln
		l.Error("Database Error: %s", err)
		return err
	}
	if n, err := result.RowsAffected(); err != nil || n == 0 {
		// Keine Zeile betroffen
		return errors.New("Not Found Error: Task does not exists")
	}
	children, err := t.GetChildren()
	if err == nil {
		for _, c := range children {
			if err := c.Delete(); err != nil {
				l.Warn("%s", err)
			}
		}
	} else {
		l.Warn("%s", err)
	}
	return nil
}

// GetChildren returns the children of a parent task
func (t *Task) GetChildren() ([]*Task, error) {
	l := m.Log("Get Children")
	children := []*Task{}
	rows, err := m.GetDatabase().Query("SELECT global_ID FROM tasks WHERE parent = ? ORDER BY `order` ASC, global_ID DESC", t.GlobalID)
	for err == nil && rows.Next() {
		var id int
		if err := rows.Scan(&id); err != nil {
			l.Error("Database Error: %s", err)
			continue
		}
		task, err := GetByGlobalID(id)
		if err != nil {
			continue
		}
		children = append(children, task)
	}
	return children, err
}

// GetSiblings returns all tasks with the same parent
func (t *Task) GetSiblings() ([]*Task, error) {
	l := m.Log("Get Siblings")
	children := []*Task{}
	rows, err := m.GetDatabase().Query("SELECT global_ID FROM tasks WHERE parent = ? ORDER BY `order` ASC, global_ID DESC", t.Parent)
	for err == nil && rows.Next() {
		var id int
		if err := rows.Scan(&id); err != nil {
			l.Error("Database Error: %s", err)
			continue
		}
		task, err := GetByGlobalID(id)
		if err != nil {
			continue
		}
		children = append(children, task)
	}
	return children, err
}

// GetParent returns the parent task
func (t *Task) GetParent() (*Task, error) {
	return GetByGlobalID(t.Parent)
}

//GetByGlobalID gibt einen Task anhand einer ID aus
func GetByGlobalID(GlobalID int) (*Task, error) {
	l := m.Log("Get Task")
	t := &Task{GlobalID: GlobalID}
	AssignedTo := ""
	row := m.GetDatabase().QueryRow("SELECT assigned_to, title, parent, description, due_date, `order`, status_task FROM tasks WHERE global_ID = ?", GlobalID)
	if err := row.Scan(&AssignedTo, &t.Title, &t.Parent, &t.Description, &t.DueDate, &t.Order, &t.Status); err != nil { // [irgendetwas] aus der aktuellen Zeile laden (in der Reihenfolge wie oben bei SELECT angegeben)
		if err != sql.ErrNoRows {
			// Fehler behandeln
			l.Error("Database Error: %s", err)
			return nil, err
		}
		// nichts gefunden
		return nil, errors.New("Not Found Error: Task does not exists")
	}
	err := json.Unmarshal([]byte(AssignedTo), &t.AssignedTo)
	if err != nil {
		l.Warn("AssignedTo Unmarshal Error: %s", err)
	}

	t.Metadata = map[string]string{}
	for _, meta := range GetAllMeta(t.GlobalID) {
		t.Metadata[meta.OptionalMetadata] = meta.ValueMetadata
	}

	return t, nil
}

//GetAllTasks gibt ein Array aus, welches alle Task beinhaltet
func GetAllTasks() []*Task {
	returnArray := []*Task{}
	l := m.Log("Get Task")

	rows, err := m.GetDatabase().Query("SELECT assigned_to, title, parent, description, due_date, global_ID, status_task, `order` FROM tasks ORDER BY `order` ASC, global_ID DESC")
	for err == nil && rows.Next() {
		t := &Task{}
		AssignedTo := ""
		if err := rows.Scan(&AssignedTo, &t.Title, &t.Parent, &t.Description, &t.DueDate, &t.GlobalID, &t.Status, &t.Order); err != nil { // [irgendetwas] aus der aktuellen Zeile laden (in der Reihenfolge wie oben bei SELECT angegeben)
			// Fehler behandeln
			l.Error("Database Error: %s", err)

		}
		err := json.Unmarshal([]byte(AssignedTo), &t.AssignedTo)
		if err != nil {
			l.Warn("AssignedTo Unmarshal Error: %s", err)
		}

		t.Metadata = map[string]string{}
		for _, meta := range GetAllMeta(t.GlobalID) {
			t.Metadata[meta.OptionalMetadata] = meta.ValueMetadata
		}

		t.Link = users.GenerateLink(nil, t.GlobalID)

		returnArray = append(returnArray, t)
	}
	return returnArray
}

// GetByGlobalIDWithChildren gibt den Task mit allen seinen Kinder zurück
func GetByGlobalIDWithChildren(GlobalID int) ([]*Task, error) {
	returnArray := []*Task{}
	l := m.Log("Get Task")

	rows, err := m.GetDatabase().Query("WITH RECURSIVE a (global_ID) AS (SELECT global_ID FROM tasks WHERE global_ID = ? UNION ALL SELECT tasks.global_ID FROM tasks, a WHERE tasks.parent  = a.global_ID) SELECT assigned_to, title, parent, description, due_date, global_ID, status_task,`order` from tasks WHERE global_ID IN a ORDER BY `order` ASC, global_ID DESC", GlobalID)
	for err == nil && rows.Next() {
		t := &Task{}
		AssignedTo := ""
		if err := rows.Scan(&AssignedTo, &t.Title, &t.Parent, &t.Description, &t.DueDate, &t.GlobalID, &t.Status, &t.Order); err != nil { // [irgendetwas] aus der aktuellen Zeile laden (in der Reihenfolge wie oben bei SELECT angegeben)
			// Fehler behandeln
			l.Error("Database Error: %s", err)
			return nil, err
		}
		err := json.Unmarshal([]byte(AssignedTo), &t.AssignedTo)
		if err != nil {
			l.Warn("AssignedTo Unmarshal Error: %s", err)
		}

		t.Metadata = map[string]string{}
		for _, meta := range GetAllMeta(t.GlobalID) {
			t.Metadata[meta.OptionalMetadata] = meta.ValueMetadata
		}

		t.Link = users.GenerateLink(nil, t.GlobalID)

		returnArray = append(returnArray, t)
	}
	if len(returnArray) == 0 {
		return nil, errors.New("Not Found Error: Task does not exists")
	}
	return returnArray, nil
}

// Change ist ein Struct für die Änderung in der Reihenfolge
type Change struct {
	Order  int `json:"Order"`
	Parent int `json:"Parent"`
}

// ReorderBelow sortiert einen Task unter eine anderen
func (t *Task) ReorderBelow(above *Task) (map[int]Change, error) {
	var reply = map[int]Change{}

	// Move above everything else
	if above == nil {
		minOrder := math.MaxInt64
		siblings, err := t.GetSiblings()
		if err != nil {
			return reply, err
		}
		for _, s := range siblings {
			if t == s {
				continue
			}
			if s.Order < minOrder {
				minOrder = s.Order
			}
		}
		t.Order = minOrder - 1
		reply[t.GlobalID] = Change{t.Order, t.Parent}
		return reply, t.Save()
	}

	if t.GlobalID == above.GlobalID {
		return reply, errors.New("Can't move a task below itself")
	}

	var err error
	var p = above
	for err == nil {
		p, err = p.GetParent()
		if p != nil && p.GlobalID == t.GlobalID {
			return map[int]Change{}, errors.New("Can't move a task into itself")
		}
	}

	t.Parent = above.Parent
	if err := t.Save(); err != nil {
		return map[int]Change{t.GlobalID: Change{t.Order, t.Parent}}, err
	}

	siblings, err := t.GetSiblings()
	if err != nil {
		return map[int]Change{t.GlobalID: Change{t.Order, t.Parent}}, err
	}
	orders := []int{} // Cache for more efficient saving
	for _, s := range siblings {
		orders = append(orders, s.Order)
	}

	t.Order = above.Order
	if t.GlobalID > above.GlobalID { // t is above above
		t.Order++
	}
	reply[t.GlobalID] = Change{t.Order, t.Parent}

	isBelow := false
	lastOrder := t.Order
	lastGID := t.GlobalID
	for _, s := range siblings {
		if s.GlobalID == t.GlobalID {
			continue
		}
		if s.GlobalID == above.GlobalID {
			isBelow = true
			continue
		}
		if isBelow {
			if s.Order < lastOrder { // lastOrder did grow, but s.Order didn't yet
				s.Order += lastOrder - s.Order
			}
			if s.Order == lastOrder && lastGID < s.GlobalID { // s is above the last element
				s.Order++
			}
			lastOrder = s.Order
		}
	}

	// Save Changed tasks
	for i, s := range siblings {
		if s.Order == orders[i] {
			continue
		}
		if _, ok := reply[s.GlobalID]; ok {
			reply[s.GlobalID] = Change{s.Order, reply[s.GlobalID].Parent}
		} else {
			reply[s.GlobalID] = Change{s.Order, s.Parent}
		}
		if err := s.Save(); err != nil {
			return reply, err
		}
	}
	if err := t.Save(); err != nil {
		return reply, err
	}
	return reply, nil
}

// ReorderInto sortiert einen Task als Kind eines anderen
func (t *Task) ReorderInto(task *Task) (map[int]Change, error) {
	var err error
	var p = task
	for err == nil {
		if p.GlobalID == t.GlobalID {
			return map[int]Change{}, errors.New("Can't move a task into itself")
		}
		p, err = p.GetParent()
	}

	t.Parent = task.GlobalID
	if err := t.Save(); err != nil {
		return map[int]Change{}, err
	}
	return t.ReorderBelow(nil)
}

/*

IDEE!

//GetMetadataToTask ....
func (t *Task) GetMetadataToTask() error {
	l := m.Log("Get Metadata to Task")

	result, err := m.GetDatabase().Exec("SELECT * FROM tasks NATURAL JOIN tasks_optional WHERE global_ID = ?", t.GlobalID)
	if err != nil {
		// Fehler behandeln
		l.Error("Database Error: %s", err)
		return err
	}
	if n, err := result.RowsAffected(); err != nil || n == 0 {
		// Keine Zeile betroffen
		return errors.New("Not Found Error: Task does not exists")
	}
	return nil
}
*/
