export default class Task {
  constructor(obj) {
    this.apply(obj);
  }

  apply(obj) {
    this.GlobalID = obj.GlobalID || null;
    this.AssignedTo = obj.AssignedTo || [];
    this.Title = obj.Title || "";
    this.Parent = obj.Parent || null;
    this.Description = obj.Description || "";
    this.DueDate = obj.DueDate ? new Date(obj.DueDate) : null;
    this.Order = obj.Order || 0;
    this.Status = obj.Status || "";
    this.Metadata = obj.Metadata || {};
    this.Link = obj.Link || "";
  }

  async save() {
    // TODO: keep hash, only save if changed
    let obj = JSON.parse(JSON.stringify(this));
    if (!obj.Title) obj.Title = "-";
    
    let r;this
    if (this.GlobalID) r = await api.PUT("/api/tasks/" + this.GlobalID, obj);
    else r = await api.POST("/api/tasks", obj);
    if (!r.ok || !r.content) throw new Error("network error: " + r.status + " " + r.statusText + "\n" + r.content);

    return r.content;
  }

  async remove() {
    const r = await api.DELETE("/api/tasks/" + this.GlobalID);
    if (!r.ok || !r.content) throw new Error("network error: " + r.status + " " + r.statusText + "\n" + r.content);
  }

  static async get(id) {
    if (!id) throw new Error("id must not be nullable");
    const r = await api.GET("/api/tasks/" + id);
    if (!r.ok || !r.content) throw new Error("network error: " + r.status + " " + r.statusText);
    return new Task(r.content);
  }

  static async all() {
    const r = await api.GET("/api/tasks");
    if (!r.ok || !r.content) throw new Error("network error: " + r.status + " " + r.statusText);

    const arr = [];
    arr.link = r.headers.get("Masp-Permanent-Link");
    console.log(r);
    for (let i = 0; i < r.content.length; i++) {
      arr.push(new Task(r.content[i]));
    }
    return arr;
  }

  static async getWithChild(id) {
    if (!id) throw new Error("id must not be nullable");
    const r = await api.GET("/api/tasks/" + encodeURIComponent(id) +"?children=true");
    if (!r.ok || !r.content) throw new Error("network error: " + r.status + " " + r.statusText + " " + r.content);
    
    const arr = [];
    arr.link = r.headers.get("Masp-Permanent-Link");
    for (let i = 0; i < r.content.length; i++) {
      arr.push(new Task(r.content[i]));
    }
    return arr;
  }
}
