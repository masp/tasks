package tasks

import (
	"github.com/gin-gonic/gin"

	"github.com/gocarina/gocsv"
)

//export exportirt alle Task als csv-Datei
func export(c *gin.Context) {
	content, err := gocsv.MarshalString(GetAllTasks())
	if err != nil {
		c.String(500, "Internal Server Error")
	}
	c.Header("Content-Description", "File Transfer")
	c.Header("Content-Disposition", "attachment; filename=masp-task.csv")
	c.Data(200, "text/csv", []byte(content))
}
