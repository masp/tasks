package tasks

import (
	"path"
	"runtime"

	"git.mo-mar.de/masp/masp"
	"github.com/coreos/go-semver/semver"
	"github.com/gin-gonic/gin"
)

// Module definiert das Modul
type Module struct{}

// M ist die Instanz des Moduls und muss immer so heißen, damit andere Module darauf zugreifen können
var M = Module{}

// m referenziert die API des Basismoduls und ist private, damit kein Modul sich als ein anderes ausgeben kann
var m = masp.RegisterModule(&M)

// Info gibt Informationen über das Modul aus (Name, ID, Version)
func (*Module) Info() (string, string, *semver.Version, string) {
	_, filename, _, _ := runtime.Caller(0)
	return "Task List", "tasks", semver.New("1.0.0"), path.Dir(filename)
}

// Routes richtet die vom Modul benötigten Routen ein
func (*Module) Routes(router *gin.RouterGroup) {
	if m.GetSetting("tasks-states") == "" {
		m.SaveSetting("tasks-states", `[
  {"name":"Offen", "value":0},
  {"name":"In Arbeit", "value":0.3, "icon":"fas fa-ellipsis-h", "style":{"background-color":"#32A2CF","color":"white"}},
  {"name":"Im Test", "value":0.7, "icon":"fas fa-vial", "style":{"background-color":"#FBB829"}},
  {"name":"Fertig", "value":1, "icon":"fas fa-check", "style":{"background-color":"#59A80F","color":"white"}}
]`)
	}

	// router.GET|POST|...
	router.GET("/", ListTask)
	router.POST("/", AddTask)
	router.GET("/:id", GetTask)
	router.PUT("/:id", PutTask)
	router.DELETE("/:id", DeleteTask)
	router.POST("/:id/move-below/:target", MoveTask(true))
	router.POST("/:id/move-into/:target", MoveTask(false))
}
