package tasks

import (
	"database/sql"
	"errors"
)

//Meta erzeugt ein Struct für jedes Metadatum, weclhes die zugewiesene GlobalID, den Inhalt in Form von OptionalMetadata und den Wert mit ValueMetadata enthält
type Meta struct {
	GlobalID         int
	OptionalMetadata string
	ValueMetadata    string
}

// addMeta fügt einen neues Metadatum hinzu, bzw überschreibt es, wenn es schon besteht
func (me *Meta) addMeta() error {
	//Siehe Doku
	l := m.Log("Save Meta")
	var err error
	var result sql.Result
	result, err = m.GetDatabase().Exec("REPLACE INTO tasks_optional (global_ID, optional_metadata,value_metadate) VALUES (?, ?,?)", me.GlobalID, me.OptionalMetadata, me.ValueMetadata)
	if err != nil {
		// Fehler behandeln
		l.Error("Database Error: %s", err)
		return err
	}
	if n, err := result.RowsAffected(); err != nil || n == 0 {
		// Keine Zeile betroffen
		return errors.New("Not Found Error: Meta does not exists")
	}
	return nil
}

//DeleteMeta führt das löschen von Metadaten aus
func (me *Meta) deleteMeta() error {
	//Siehe Doku
	l := m.Log("Delete Metadata")

	result, err := m.GetDatabase().Exec("DELETE FROM tasks_optional WHERE global_ID = ? AND optional_metadata = ?", me.GlobalID, me.OptionalMetadata)
	if err != nil {
		// Fehler behandeln
		l.Error("Database Error: %s", err)
		return err
	}
	if n, err := result.RowsAffected(); err != nil || n == 0 {
		// Keine Zeile betroffen
		return errors.New("Not Found Error: Metadata does not exists")
	}
	return nil
}

//GetAllMeta gibt alle Metadaten zurück für eine gegebene Global ID
func GetAllMeta(GlobalID int) []*Meta {
	returnArray := []*Meta{}
	l := m.Log("Get Metadate")

	rows, err := m.GetDatabase().Query("SELECT global_ID, optional_metadata, value_metadate FROM tasks_optional where global_ID = ?", GlobalID)
	for err == nil && rows.Next() {
		t := &Meta{}
		if err := rows.Scan(&t.GlobalID, &t.OptionalMetadata, &t.ValueMetadata); err != nil { // [irgendetwas] aus der aktuellen Zeile laden (in der Reihenfolge wie oben bei SELECT angegeben)
			// Fehler behandeln
			l.Error("Database Error: %s", err)

		}
		returnArray = append(returnArray, t)
	}
	return returnArray
}
