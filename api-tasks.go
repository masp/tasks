package tasks

import (
	"strconv"
	"strings"

	"git.mo-mar.de/masp/users"

	"github.com/gin-gonic/gin"
)

// GetTask führt auf API-Ebene die Ausgabe und Fehlerbehandlung für einen Task aus
func GetTask(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))

	if !users.HasLinkAccess(c, id) { // Verschachtelt, da Require User sonst den Statuscode setzt
		if !users.RequireUser(c) {
			return
		}
	}

	if c.Param("id") == "export.csv" {
		export(c)
		return
	}

	if id == 0 {
		c.String(400, "need an id")
		return
	}

	c.Header("Masp-Permanent-Link", users.GenerateLink(c, id))

	if c.Query("children") == "true" {
		task, err := GetByGlobalIDWithChildren(id)
		if err != nil && strings.HasPrefix(err.Error(), "Not Found Error:") {
			c.String(404, err.Error())
		} else if err != nil {
			c.String(500, err.Error())
			m.Log("Get Task").Warn("%s", err)
		} else {
			c.JSON(200, task)
		}

		return
	}
	task, err := GetByGlobalID(id)

	if err != nil && strings.HasPrefix(err.Error(), "Not Found Error:") {
		c.String(404, err.Error())
	} else if err != nil {
		c.String(500, err.Error())
		m.Log("Get Task").Warn("%s", err)
	} else {
		c.JSON(200, task)
	}
}

//PutTask überschreibt einen bestehenden Task
//Weiterhin führt zudem die Felhler behandlung für das Speichern durch
func PutTask(c *gin.Context) {
	if !users.RequireUser(c) {
		return
	}
	id, _ := strconv.Atoi(c.Param("id"))
	if id == 0 {
		c.String(400, "need an id")
		return
	}

	task := &Task{}
	err := c.BindJSON(task)
	if err != nil {
		c.String(400, err.Error())
	}
	task.GlobalID = id
	err = task.Save()
	if err != nil && strings.HasPrefix(err.Error(), "Not Found Error:") {
		c.String(404, err.Error())
	} else if err != nil && strings.HasPrefix(err.Error(), "Input Error:") {
		c.String(400, err.Error())
	} else if err != nil {
		c.String(500, err.Error())
		m.Log("Get Task").Warn("%s", err)
	} else {
		c.JSON(200, task)
	}
}

//ListTask listet alle bestehenden Tasks aus
func ListTask(c *gin.Context) {
	_, module, _, _ := M.Info()
	if !users.HasLinkAccess(c, m.GetPrefixByModule(module)) {
		if !users.RequireUser(c) {
			return
		}
	}
	c.Header("Masp-Permanent-Link", users.GenerateLink(c, m.GetPrefixByModule(module)))
	for _, t := range c.Accepted {
		if t == "text/comma-separated-values" {
			export(c)
			return
		}
	}
	c.JSON(200, GetAllTasks())
}

//AddTask speichert einen neuen Task in die Datenbank und führt die Fehlerbehandlung durch
func AddTask(c *gin.Context) {
	if !users.RequireUser(c) {
		return
	}
	task := &Task{}
	err := c.BindJSON(task)
	if err != nil {
		c.String(400, err.Error())
	}
	err = task.Save()
	if err != nil && strings.HasPrefix(err.Error(), "Not Found Error:") {
		c.String(404, err.Error())
	} else if err != nil && strings.HasPrefix(err.Error(), "Input Error:") {
		c.String(400, err.Error())
	} else if err != nil {
		c.String(500, err.Error())
		m.Log("Get Task").Warn("%s", err)
	} else {
		c.JSON(200, task)
	}
}

//DeleteTask löscht einen Task aus der Datenbak und führt eine Fehlerbehandlung durch
func DeleteTask(c *gin.Context) {
	if !users.RequireUser(c) {
		return
	}

	id, _ := strconv.Atoi(c.Param("id"))
	if id == 0 {
		c.String(400, "need an id")
		return
	}
	task, err := GetByGlobalID(id)

	if err != nil && strings.HasPrefix(err.Error(), "Not Found Error:") {
		c.String(404, err.Error())
	} else if err != nil {
		c.String(500, err.Error())
		m.Log("Get Task").Warn("%s", err)
	} else {
		err = task.Delete()
		if err != nil {
			c.String(500, err.Error())
			m.Log("Get Task").Warn("%s", err)

		} else {
			c.String(200, "Done")
		}

	}

}

//MoveTask verschiebt einen Task
func MoveTask(below bool) func(*gin.Context) {
	return func(c *gin.Context) {
		if !users.RequireUser(c) {
			return
		}
		id, _ := strconv.Atoi(c.Param("id"))
		if id == 0 {
			c.String(400, "need an id")
			return
		}
		targetID, _ := strconv.Atoi(c.Param("target"))
		if targetID == 0 && !below {
			c.String(400, "need a target") // Can be empty or 0 to move to start, but only on MoveBelow
			return
		}
		t, err1 := GetByGlobalID(id)
		target, err2 := GetByGlobalID(targetID)
		if err1 != nil && strings.HasPrefix(err1.Error(), "Not Found Error:") {
			c.String(404, err1.Error())
			return
		} else if targetID != 0 && err2 != nil && strings.HasPrefix(err2.Error(), "Not Found Error:") {
			c.String(404, "Target "+err2.Error())
			return
		} else if err1 != nil {
			c.String(500, err1.Error())
			m.Log("Get Task").Warn("%s", err1)
			return
		} else if targetID != 0 && err2 != nil {
			c.String(500, err2.Error())
			m.Log("Get Task").Warn("%s", err2)
			return
		}
		var changes map[int]Change
		if below {
			if targetID == 0 {
				t.Parent = 0
				err1 = t.Save()
			}
			if err1 == nil {
				changes, err1 = t.ReorderBelow(target)
			}
		} else {
			changes, err1 = t.ReorderInto(target)
		}

		if err1 != nil {
			c.String(500, err1.Error())
			m.Log("Get Task").Warn("%s", err1)
		} else {
			c.JSON(200, changes)
		}
	}
}
