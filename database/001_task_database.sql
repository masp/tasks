-- +goose Up
-- SQL in this section is executed when the migration is applied.
-- SQL create für alle erstes mal starten
-- Module sollen auch funktionen, wenn sie keine Datenbank --> soll kein Fehler werfen

CREATE TABLE IF NOT EXISTS tasks (
    global_ID INT NOT NULL PRIMARY KEY,
    assigned_to text,
    title text NOT NULL,
    parent INT DEFAULT NULL,
    description text,
    due_date text,
    `order` INT NOT NULL,
    status_task text,
    FOREIGN KEY (global_ID) REFERENCES base_global_ids (global_ID),
    FOREIGN KEY (parent) REFERENCES tasks (global_ID) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS tasks_optional (
    global_ID INT NOT NULL,
    optional_metadata text NOT NULL,
    value_metadate text,

    FOREIGN KEY (global_ID) REFERENCES tasks (global_ID) ON DELETE CASCADE,
    PRIMARY KEY (global_ID, optional_metadata)
   );
